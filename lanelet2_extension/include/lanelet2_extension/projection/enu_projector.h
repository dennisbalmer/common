/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Authors: Simon Thompson, Ryohsuke Mitsudome
 */

#ifndef LANELET2_EXTENSION_PROJECTION_ENU_PROJECTOR_H
#define LANELET2_EXTENSION_PROJECTION_ENU_PROJECTOR_H

#include <GeographicLib/Geocentric.hpp>
#include <lanelet2_io/Exceptions.h>
#include <lanelet2_io/Projection.h>
#include <tf2/LinearMath/Transform.h>

#include <string>
#include <utility>

namespace lanelet
{
namespace projection
{
class EnuProjector : public Projector
{
public:
  explicit EnuProjector(Origin origin);
  explicit EnuProjector(tf2::Transform origin_frame_tf);

  /**
   * [EnuProjector::forward projects gps lat/lon to ENU local frame based on Origin]
   * @param  gps [point with latitude longitude information]
   * @return     [projected point in ENU coordinates]
   */
  BasicPoint3d forward(const GPSPoint& gps) const override;

  // /**
  //  * [EnuProjector::reverse projects ENU point into gps lat/lon (WGS84)]
  //  * @param  enu_point [3d point in ENU coordinates]
  //  * @return           [projected point in WGS84]
  //  */
  GPSPoint reverse(const BasicPoint3d& enu_point) const override;

private:
  tf2::Transform getEcefToEnuTransform(double latitude, double longitude, double height);

  GeographicLib::Geocentric earth_;
  tf2::Transform ecef_enu_tf_;
};

}  // namespace projection
}  // namespace lanelet

#endif  // LANELET2_EXTENSION_PROJECTION_ENU_PROJECTOR_H
